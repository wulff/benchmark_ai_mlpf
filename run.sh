#!/bin/sh

while getopts ho: opt
do
    case "${opt}" in
        o) outputdir=${OPTARG}
           echo "Output dir: $outputdir"
        ;;
        h) echo "Usage: run.sh -o <output_dir>"
           exit
        ;;
    esac
done

echo "which python:"
which python3

# Setup
git submodule init
git submodule update

cd particleflow
git submodule init
git submodule update

cd hep_tfds
python3 setup.py install
cd ..

# Install dependencies
python3 -m pip install --upgrade setuptools
python3 -m pip install tensorflow==2.6 setGPU tensorflow-estimator==2.6 \
    sklearn matplotlib mplhep pandas scipy uproot3 uproot3-methods \
    keras==2.6.0 awkward0 keras-tuner networkx \
    tensorflow-probability==0.14.1 tensorflow-addons==0.15.0 tensorboard==2.6\
    tqdm click tensorflow-datasets 'ray[default]' 'ray[tune]' \
    tf-models-official tensorflow-text tensorflow-datasets==4.4.0 \
    tf2onnx onnxruntime zenodo_get seaborn scikit-optimize nevergrad

# Run training workflow
python3 mlpf/pipeline.py train -c parameters/delphes-bench.yaml --nepochs 10 -p run_ --plot-freq 1000000 -b $outputdir
