#!/bin/sh

#SBATCH --time=01:00:00
#SBATCH --nodes=1
#SBATCH --tasks-per-node=1

#SBATCH --cpus-per-task=40
#SBATCH --constraint=skylake

#SBATCH --job-name=mlpf_intel

#SBATCH --output=logs/%x_%j.out
#SBATCH --error=logs/%x_%j.err


# Add jobscript to job output
echo "#################### Job submission script. #############################"
cat $0
echo "################# End of job submission script. #########################"

# Load dependencies
module purge
module load gcc singularity

# Run benchmark container
singularity run -c -B /mnt/home/$USER/mlpf/intel:/results -B /tmp /mnt/home/$USER/images/mlpf_intel.sif --nepochs 5 --batch_size 10 --num_devices 10
