#!/bin/sh

# Walltime limit
#SBATCH -t 1-00:00:00
#SBATCH -N 1
#SBATCH --exclusive
#SBATCH --tasks-per-node=1
#SBATCH -p gpu
#SBATCH --gpus-per-task=4
#SBATCH --constraint=a100,sxm4

# Job name
#SBATCH -J bench

# Output and error logs
#SBATCH -o logs_slurm/log_%x_%j.out
#SBATCH -e logs_slurm/log_%x_%j.err

# Add jobscript to job output
echo "#################### Job submission script. #############################"
cat $0
echo "################# End of job submission script. #########################"

module purge
module load slurm gcc cuda/11.1.0_455.23.05 cuda/11.4.2 cudnn/8.2.4.15-11.4 git/2.31.1
nvidia-smi

source ~/miniconda3/bin/activate benchtest
which python3
python3 --version

CUDA_VISIBLE_DEVICES=0,1,2,3 bash run.sh -o sbatch_benchmark_results
