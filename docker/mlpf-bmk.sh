#!/bin/bash

set -e

if [ -f /run/.containerenv ]; then FLAVOR="podman"
elif [ -f /.dockerenv ]; then FLAVOR="docker"
elif [ -f /singularity ]; then FLAVOR="singularity"
else FLAVOR="unknown";
fi

# Default config
NEPOCHS=10
NTRAIN=0      # 0 is None
NTEST=0       # 0 is None
BSIZE=0       # 0 is Default
NDEVICES=0    # 0 is Default
RESULTSDIR=/results
DESCRIPTION="Machine Learning Particle Flow (MLPF) benchmark"

log() {
  case $1 in
    error)  shift 1; echo -e "\e[31m>>> ERROR:\e[0m $*\n" | tee -a $RESULTSDIR/out.log ; exit 2 ;;
    info)   shift 1; echo -e "\e[34m$*\e[0m\n" | tee -a $RESULTSDIR/out.log ;;
    silent) shift 1; echo "$*" >> $RESULTSDIR/out.log ;;
    *)      echo "$*" | tee -a $RESULTSDIR/out.log ;
  esac
}

usage() {
  echo "Usage: $0 [options]"
  echo "Options:"
  echo "-h, --help              Prints this message and exit."
  echo "-w, --resultsdir <str>  Results directory.        Default: $RESULTSDIR"
  echo "-e, --nepochs <int>     Number of epochs.         Default: $NEPOCHS"
  echo "-B, --batch_size <int>  Batch size per device.    Default: $BSIZE"
  echo "-D, --num_devices <int> Number of devices to use. Default: $NDEVICES"
  echo "    --ntrain <int>      Train steps limit.        Default: $NTRAIN"
  echo "    --ntest <int>       Test steps limit.         Default: $NTEST"
  exit 0
}

parse_args() {
  options=$(getopt --long resultsdir:,nepochs:,ntrain:,ntest:,batch_size:,num_devices:,help -o wWeDB:h -- "$@")
  if [ $? != 0 ]; then echo "Invalid options provided." >&2; usage; fi
  eval set -- "$options"
  while true; do
    case $1 in
      --help | -h ) usage; exit 0;;
      --resultsdir | -w ) RESULTSDIR=$2; shift ;;
      --ntrain ) NTRAIN=$2; shift ;;
      --ntest ) NTEST=$2; shift ;;
      --nepochs | -e ) NEPOCHS=$2; shift ;;
      --num_devices | -D ) NDEVICES=$2; shift ;;
      --batch_size | -B ) BSIZE=$2; shift ;;
      -- ) shift; break;;
    esac
    shift
  done
}

parse_args $*

if [ -f "$RESULTSDIR"/out.log ]; then rm "$RESULTSDIR"/out.log; fi
log info "Base working directory: $RESULTSDIR"
log info "Running benchmark MLPF"

cd /workspace/particleflow/
python3 mlpf/pipeline.py train \
  --config parameters/delphes-benchmark.yaml \
  --prefix /tmp/train_ \
  --plot-freq 1000000 \
  --benchmark_dir $RESULTSDIR \
  --num_devices $NDEVICES \
  --batch_size $BSIZE \
  --nepochs $NEPOCHS \
  --ntrain $NTRAIN \
  --ntest $NTEST

REPORT=$(cat $RESULTSDIR/result.json)

generate_json() {
  jq -n \
    --argjson nepochs "$NEPOCHS" \
    --argjson report "$REPORT" \
    --arg containment "$FLAVOR" \
    --arg description "$DESCRIPTION" \
    '{
      "run_info":{
        $nepochs
      },
      $report,
      "app":{
        $containment,
        $description
      }
    }'
}

generate_json > $RESULTSDIR/mlpf-report.json
log info "Finished running MLPF"
