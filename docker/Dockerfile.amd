# AMD CPU - use default tensorflow
FROM tensorflow/tensorflow:2.6.0

# Set the environment
ENV LC_ALL=C.UTF-8
ENV LANG=C.UTF-8

# Install some basic utilities
RUN apt update && apt install -y jq && apt clean && rm -rf /var/lib/apt/lists/*

# Prepare the workspace folder
RUN mkdir -p /workspace
ENV PATH=/workspace:$PATH

# Get the source code for MLPF
COPY particleflow /workspace/particleflow

# Get the data
COPY tensorflow_datasets/delphes_pf /workspace/tensorflow_datasets/delphes_pf

# Copy in the dependencies
COPY ./requirements.txt /workspace/
COPY ./docker/mlpf-bmk.sh /workspace/

# Install dependencies
RUN python3 -m pip install --upgrade pip==21.3.1 setuptools==59.5.0
RUN python3 -m pip install --no-cache-dir -r /workspace/requirements.txt
RUN python3 -m pip install --no-cache-dir /workspace/particleflow/hep_tfds

ENTRYPOINT ["/workspace/mlpf-bmk.sh"]
