#!/bin/bash
singularity run --ipc --pid \
                --home $PWD:/srv \
                --bind /cvmfs \
                --disable-cache \
                oras://registry.cern.ch/mlpf/main:latest
