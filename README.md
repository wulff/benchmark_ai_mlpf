# Containerized training workflow of MLPF
Containerized training workflow of an AI model for Particle-Flow reconstruction in High Energy Physics.

How to build and run the container:

1. Clone this repo and `cd` into it.
2. `git submodule init && git submodule update`
3. `cd particleflow && git submodule init && git submodule update && cd ..`
4. `./docker/setup_docker.sh`
5. `docker run mlpf:<tag>`

If you want to modify the training workflow, change the `docker/mlpf-bmk.sh` script and/or the configuration file `particleflow/parameters/delphes-benchmark.yaml`

## Singularity

1. `./singularity/setup_singularity.sh`
2. `singularity run mlpf.sif`

# Non-containerized training workflow of MLPF
Alternatively, the benchmark can be run by:
1. Make sure you have git lfs installed.
2. Clone this repo and `cd` into it.
3. Activate a fresh virtual environment with Python 3.9.
4. Execute the `run.sh` script.
